import React from 'react';

import {
  NativeSelect,
  type SelectItem,
  type NativeSelectProps,
} from '@mantine/core';

import type {
  NameLabelPair,
  VersatileSelectorProps,
} from '@react-querybuilder/ts';

type MantineValueSelectorProps = VersatileSelectorProps &
  Omit<NativeSelectProps, 'data'>;

export function MantineValueSelector({
  className,
  handleOnChange,
  options,
  value,
  title,
  disabled,
  testID: _testID,
  rules: _rules,
  level: _level,
  path: _path,
  context: _context,
  validation: _validation,
  operator: _operator,
  field: _field,
  fieldData: _fieldData,
  multiple: _multiple,
  listsAsArrays: _listsAsArrays,
  ...props
}: MantineValueSelectorProps) {
  const optionData = React.useMemo<SelectItem[]>(() => {
    if (options) {
      const opts = options as NameLabelPair[];

      return opts.map<SelectItem>(({ name, label }) => {
        return {
          value: name,
          label,
        };
      });
    }
    return [];
  }, [options]);

  return (
    <NativeSelect
      className={className}
      title={title}
      value={value}
      size="xs"
      variant="filled"
      disabled={disabled}
      onChange={handleOnChange}
      {...props}
      data={optionData}
    />
  );
}

MantineValueSelector.displayName = 'MantineValueSelector';
