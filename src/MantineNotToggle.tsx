import React from 'react';

import { Switch, type SwitchProps } from '@mantine/core';

import type { NotToggleProps } from '@react-querybuilder/ts';

type MantineNotToggleProps = NotToggleProps & SwitchProps;

export function MantineNotToggle({
  className,
  handleOnChange,
  label,
  checked,
  title,
  disabled,
  path: _path,
  context: _context,
  validation: _validation,
  testID: _testID,
  ...props
}: MantineNotToggleProps) {
  const id = React.useRef(`notToggle-${Math.random()}`);

  return (
    <div style={{ display: 'inline-block' }}>
      <Switch
        id={id.current}
        className={className}
        title={title}
        disabled={disabled}
        size="sm"
        checked={checked}
        label={label}
        onChange={(e) => handleOnChange(e.target.checked)}
        {...props}
      />
    </div>
  );
}

MantineNotToggle.displayName = 'MantineNotToggle';
