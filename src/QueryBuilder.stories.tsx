import React from 'react';

import { ComponentStory, ComponentMeta } from '@storybook/react';

import {
  defaultOperators,
  formatQuery,
  type Field,
  type ExportFormat,
  QueryBuilder,
  type QueryBuilderProps,
  type OptionGroup,
  type ParameterizedNamedSQL,
  type ParameterizedSQL,
  type RuleGroupType,
  type RuleType,
} from 'react-querybuilder';
import 'react-querybuilder/dist/query-builder.scss';

import { mantineControlElements } from '.';

type CustomQueryBuilderProps = QueryBuilderProps & {
  // eslint-disable-next-line react/require-default-props
  exportFormat?: ExportFormat;
  // eslint-disable-next-line react/require-default-props
  parseNumbers?: boolean;
  // eslint-disable-next-line react/require-default-props
  onFormattedQueryChange?: (
    q: string | ParameterizedSQL | ParameterizedNamedSQL
  ) => void;
};

function CustomQueryBuilder({
  exportFormat,
  onFormattedQueryChange,
  ...props
}: CustomQueryBuilderProps) {
  return <QueryBuilder {...props} />;
}

const validator = (r: RuleType) => !!r.value;

const musicalInstruments: OptionGroup[] = [
  {
    label: 'Percussion instruments',
    instruments: [
      'Clapstick',
      'Cowbell',
      'Cymbal',
      'Gong',
      'Maraca',
      'Marimba',
      'Spoon',
      'Steelpan',
      'Tambourine',
      'Triangle',
      'Vibraphone',
      'Washboard',
      'Wood block',
      'Wooden fish',
      'Xylophone',
    ],
  },
  {
    label: 'Membranophones',
    instruments: [
      'Barrel drum',
      'Bass drum',
      'Bongo drums',
      'Conga',
      'Drum',
      'Drum kit',
      "Jew's harp",
      'Octaban',
      'Samphor',
      'Snare drum',
      'Timpani',
      'Tom-tom',
    ],
  },
  {
    label: 'Wind instruments',
    instruments: [
      'Accordion',
      'Air horn',
      'Bagpipe',
      'Baritone horn',
      'Bassoon',
      'Bazooka',
      'Beatboxing',
      'Blown bottle',
      'Bugle',
      'Clarinet',
      'Conch',
      'Cornet',
      'Didgeridoo',
      'Double bell euphonium',
      'Doulophone',
      'English horn',
      'Euphonium',
      'Flugelhorn',
      'Flute',
      'French horn',
      'Harmonica',
      'Irish flute',
      'Jug',
      'Kazoo',
      'Melodeon',
      'Mezzo-soprano',
      'Oboe',
      'Ocarina',
      'Pan flute',
      'Piccolo',
      'Pipe organ',
      'Recorder',
      'Saxophone',
      'Slide whistle',
      'Sousaphone',
      'Trombone',
      'Trumpet',
      'Tuba',
      'Whistle',
    ],
  },
  {
    label: 'Stringed instruments',
    instruments: [
      'Aeolian harp',
      'Bandolin',
      'Banjo ukulele',
      'Cello',
      'Chapman stick',
      'Clavichord',
      'Clavinet',
      'Double bass',
      'Dulcimer',
      'Fiddle',
      'Guitar',
      'Hammered dulcimer',
      'Harp',
      'Harpsichord',
      'Lute',
      'Lyre',
      'Maguhu',
      'Mandola',
      'Mandolin',
      'Octobass',
      'Piano',
      'Sitar',
      'Ukulele',
      'Viol',
      'Violin',
      'Washtub bass',
    ],
  },
  {
    label: 'Electronic instruments',
    instruments: [
      'AlphaSphere',
      'Audiocubes',
      'Bass pedals',
      'Continuum Fingerboard',
      'Croix Sonore',
      "Denis d'or",
      'Dubreq stylophone',
      'Drum machine',
      'Eigenharp',
      'Electric guitar',
      'Electronic keyboard',
      'Electronic organ',
      'EWI',
      'Fingerboard synthesizer',
      'Hammond organ',
      'Keyboard',
      'Keytar',
      'Kraakdoos',
      'Laser harp',
      'Mellotron',
      'MIDI keyboard',
      'Omnichord',
      'Ondes Martenot',
      'Otamatone',
      'Sampler',
      'Seaboard music instrument',
      'Skoog',
      'Synclavier',
      'Synthesizer',
      'Teleharmonium',
      'Tenori-on',
      'Theremin',
      'trautonium',
      'Turntablism',
      'Turntable',
    ],
  },
].map(({ label, instruments }) => ({
  label,
  options: instruments.map((s) => ({ name: s, label: s })),
}));

const fields: Field[] = [
  {
    name: 'firstName',
    label: 'First Name',
    placeholder: 'Enter first name',
    validator,
  },
  {
    name: 'lastName',
    label: 'Last Name',
    placeholder: 'Enter last name',
    defaultOperator: 'beginsWith',
    validator,
  },
  { name: 'age', label: 'Age', inputType: 'number', validator },
  {
    name: 'isMusician',
    label: 'Is a musician',
    valueEditorType: 'checkbox',
    operators: defaultOperators.filter((op) => op.name === '='),
    defaultValue: false,
  },
  {
    name: 'instrument',
    label: 'Primary instrument',
    valueEditorType: 'select',
    values: musicalInstruments,
    defaultValue: 'Piano',
    operators: defaultOperators.filter(
      (op) => op.name === '=' || op.name === 'in'
    ),
  },
  {
    name: 'alsoPlaysInstruments',
    label: 'Also plays instruments',
    valueEditorType: 'multiselect',
    values: musicalInstruments,
    defaultValue: 'Guitar',
    operators: defaultOperators.filter((op) => op.name === 'in'),
  },
  {
    name: 'gender',
    label: 'Gender',
    operators: defaultOperators.filter((op) => op.name === '='),
    valueEditorType: 'radio',
    values: [
      { name: 'M', label: 'Male' },
      { name: 'F', label: 'Female' },
      { name: 'O', label: 'Other' },
    ],
  },
  { name: 'height', label: 'Height', validator },
  { name: 'job', label: 'Job', validator },
  { name: 'description', label: 'Description', valueEditorType: 'textarea' },
  { name: 'birthdate', label: 'Birth Date', inputType: 'date' },
  { name: 'datetime', label: 'Show Time', inputType: 'datetime-local' },
  { name: 'alarm', label: 'Daily Alarm', inputType: 'time' },
  {
    name: 'groupedField1',
    label: 'Grouped Field 1',
    comparator: 'group',
    group: 'group1',
    valueSources: ['field', 'value'],
  },
  {
    name: 'groupedField2',
    label: 'Grouped Field 2',
    comparator: 'group',
    group: 'group1',
    valueSources: ['field', 'value'],
  },
  {
    name: 'groupedField3',
    label: 'Grouped Field 3',
    comparator: 'group',
    group: 'group1',
    valueSources: ['field', 'value'],
  },
  {
    name: 'dateField1',
    label: 'Date Field 1',
    inputType: 'date',
    validator,
  },
];

const initialQuery: RuleGroupType = {
  combinator: 'and',
  not: false,
  rules: [
    {
      field: 'firstName',
      value: 'Stev',
      operator: 'beginsWith',
    },
    {
      field: 'lastName',
      value: 'Vai, Vaughan',
      operator: 'in',
    },
    {
      field: 'age',
      operator: '>',
      value: 28,
    },
    {
      combinator: 'or',
      rules: [
        {
          field: 'isMusician',
          operator: '=',
          value: true,
        },
        {
          field: 'instrument',
          operator: '=',
          value: 'Guitar',
        },
      ],
    },
    {
      field: 'groupedField1',
      operator: '=',
      value: 'groupedField4',
      valueSource: 'field',
    },
    {
      field: 'dateField1',
      operator: '=',
      value: '1990-01-02',
    },
  ],
};

export default {
  argTypes: {
    addRuleToNewGroups: {
      control: { type: 'boolean' },
    },
    autoSelectField: {
      control: { type: 'boolean' },
    },
    autoSelectOperator: {
      control: { type: 'boolean' },
    },
    debugMode: {
      control: { type: 'boolean' },
    },
    enableDragAndDrop: {
      control: { type: 'boolean' },
    },
    exportFormat: {
      options: [
        'json',
        'sql',
        'json_without_ids',
        'parameterized',
        'parameterized_named',
        'mongodb',
        'cel',
      ],
      control: { type: 'select' },
    },
    independentCombinators: {
      control: { type: 'boolean' },
    },
    listsAsArrays: {
      control: { type: 'boolean' },
    },
    resetOnFieldChange: {
      control: { type: 'boolean' },
    },
    resetOnOperatorChange: {
      control: { type: 'boolean' },
    },
    showCloneButtons: {
      control: { type: 'boolean' },
    },
    showLockButtons: {
      control: { type: 'boolean' },
    },
    showCombinatorsBetweenRules: {
      control: { type: 'boolean' },
    },
    showNotToggle: {
      control: { type: 'boolean' },
    },
    onFormattedQueryChange: {
      action: 'onFormattedQueryChange',
    },
  },
} as ComponentMeta<typeof CustomQueryBuilder>;

// eslint-disable-next-line react/function-component-definition
const BasicTemplate: ComponentStory<typeof CustomQueryBuilder> = ({
  addRuleToNewGroups,
  autoSelectField,
  autoSelectOperator,
  debugMode,
  enableDragAndDrop,
  exportFormat,
  independentCombinators,
  listsAsArrays,
  parseNumbers,
  resetOnFieldChange,
  resetOnOperatorChange,
  showCloneButtons,
  showCombinatorsBetweenRules,
  showLockButtons,
  showNotToggle,
  onFormattedQueryChange,
}: CustomQueryBuilderProps) => {
  const [query, setQuery] = React.useState(initialQuery);

  const processQuery = React.useCallback(
    (
      q: RuleGroupType<RuleType<string, string, any>, string>
    ): ParameterizedSQL => {
      return formatQuery(q, {
        // @ts-ignore
        format: exportFormat,
        parseNumbers,
      });
    },
    [exportFormat, parseNumbers]
  );

  const onQueryChangeHandler: CustomQueryBuilderProps['onQueryChange'] = (
    q
  ) => {
    setQuery(q);
  };

  React.useEffect(() => {
    onFormattedQueryChange?.(processQuery(query));
  }, [exportFormat, onFormattedQueryChange, processQuery, query]);

  return (
    <CustomQueryBuilder
      addRuleToNewGroups={addRuleToNewGroups}
      autoSelectField={autoSelectField}
      autoSelectOperator={autoSelectOperator}
      controlElements={mantineControlElements}
      debugMode={debugMode}
      enableDragAndDrop={enableDragAndDrop}
      fields={fields}
      independentCombinators={independentCombinators}
      query={query}
      listsAsArrays={listsAsArrays}
      resetOnFieldChange={resetOnFieldChange}
      resetOnOperatorChange={resetOnOperatorChange}
      showCloneButtons={showCloneButtons}
      showCombinatorsBetweenRules={showCombinatorsBetweenRules}
      showNotToggle={showNotToggle}
      showLockButtons={showLockButtons}
      onQueryChange={onQueryChangeHandler}
    />
  );
};

export const Basic = BasicTemplate.bind({});

Basic.args = {
  addRuleToNewGroups: false,
  autoSelectField: true,
  autoSelectOperator: true,
  debugMode: false,
  enableDragAndDrop: false,
  exportFormat: 'json',
  independentCombinators: false,
  listsAsArrays: false,
  parseNumbers: false,
  resetOnFieldChange: true,
  resetOnOperatorChange: false,
  showCloneButtons: false,
  showCombinatorsBetweenRules: false,
  showLockButtons: false,
  showNotToggle: false,
};
