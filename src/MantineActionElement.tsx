import React from 'react';

import { Button, type ButtonProps } from '@mantine/core';

import type { ActionWithRulesProps } from '@react-querybuilder/ts';

type MantineActionElementProps = ActionWithRulesProps & ButtonProps;

export function MantineActionElement({
  className,
  handleOnClick,
  label,
  title,
  disabled,
  disabledTranslation,
  testID: _testID,
  rules: _rules,
  level: _level,
  path: _path,
  context: _context,
  validation: _validation,
  ...props
}: MantineActionElementProps) {
  return (
    <Button
      className={className}
      title={
        disabledTranslation && disabled ? disabledTranslation.title : title
      }
      variant="default"
      onClick={handleOnClick}
      size="xs"
      disabled={disabled && !disabledTranslation}
      {...props}
    >
      {disabledTranslation && disabled ? disabledTranslation.label : label}
    </Button>
  );
}

MantineActionElement.displayName = 'MantineActionElement';
