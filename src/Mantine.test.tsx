import React from 'react';

import { MantineProvider } from '@mantine/core';

import type { DragHandleProps } from 'react-querybuilder';

import {
  testActionElement,
  testDragHandle,
  testNotToggle,
  testValueEditor,
  testValueSelector,
} from '../genericTests';

import {
  MantineActionElement,
  MantineDragHandle,
  MantineNotToggle,
  MantineValueEditor,
  MantineValueSelector,
} from '.';

const generateWrapper = (RQBComponent: any) => {
  function Wrapper(props: React.ComponentPropsWithoutRef<typeof RQBComponent>) {
    return (
      <MantineProvider>
        <RQBComponent {...props} />
      </MantineProvider>
    );
  }

  Wrapper.displayName = RQBComponent.displayName;

  return Wrapper;
};

const WrapperDH = React.forwardRef<HTMLSpanElement, DragHandleProps>(
  (props, ref) => (
    <MantineProvider>
      <MantineDragHandle {...props} ref={ref} />
    </MantineProvider>
  )
);
WrapperDH.displayName = MantineDragHandle.displayName;

testActionElement(generateWrapper(MantineActionElement));
testDragHandle(WrapperDH);
testNotToggle(generateWrapper(MantineNotToggle));
testValueEditor(generateWrapper(MantineValueEditor));
testValueSelector(generateWrapper(MantineValueSelector), { multi: true });
