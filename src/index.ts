import type { Controls } from '@react-querybuilder/ts';

import { MantineActionElement } from './MantineActionElement';
import { MantineDragHandle } from './MantineDragHandle';
import { MantineNotToggle } from './MantineNotToggle';
import { MantineValueEditor } from './MantineValueEditor';
import { MantineValueSelector } from './MantineValueSelector';

export const mantineControlElements: Partial<Controls> = {
  addGroupAction: MantineActionElement,
  addRuleAction: MantineActionElement,
  cloneGroupAction: MantineActionElement,
  cloneRuleAction: MantineActionElement,
  combinatorSelector: MantineValueSelector,
  dragHandle: MantineDragHandle,
  fieldSelector: MantineValueSelector,
  notToggle: MantineNotToggle,
  operatorSelector: MantineValueSelector,
  lockRuleAction: MantineActionElement,
  lockGroupAction: MantineActionElement,
  removeGroupAction: MantineActionElement,
  removeRuleAction: MantineActionElement,
  valueEditor: MantineValueEditor,
  valueSourceSelector: MantineValueSelector,
};

export {
  MantineActionElement,
  MantineDragHandle,
  MantineNotToggle,
  MantineValueEditor,
  MantineValueSelector,
};
