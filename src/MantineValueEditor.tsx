import React from 'react';

import {
  Checkbox,
  Input,
  NumberInput,
  Radio,
  type RadioProps,
  Switch,
  Textarea,
} from '@mantine/core';

import { DatePicker } from '@mantine/dates';

import {
  joinWith,
  standardClassnames,
  toArray,
  useValueEditor,
  type ValueEditorProps,
} from 'react-querybuilder';

import dayjs from 'dayjs';

import { MantineValueSelector } from './MantineValueSelector';

export type RadioItemValue = Pick<RadioProps, 'label' | 'value'>;

export function MantineValueEditor({
  fieldData,
  operator,
  value,
  handleOnChange,
  title,
  className,
  type,
  inputType,
  values = [],
  listsAsArrays,
  valueSource: _vs,
  testID,
  disabled,
  ...props
}: ValueEditorProps) {
  useValueEditor({ handleOnChange, inputType, operator, value });
  if (operator === 'null' || operator === 'notNull') {
    return null;
  }

  const placeholder = fieldData?.placeholder ?? '';
  const inputTypeCoerced = ['between', 'notBetween', 'in', 'notIn'].includes(
    operator
  )
    ? 'text'
    : inputType ?? 'text';

  if (
    (operator === 'between' || operator === 'notBetween') &&
    type === 'select'
  ) {
    const valArray = toArray(value);
    const selector1Handler = (v: string) => {
      const val = [v, valArray[1] ?? values[0]?.name, ...valArray.slice(2)];
      handleOnChange(listsAsArrays ? val : joinWith(val, ','));
    };
    const selector2Handler = (v: string) => {
      const val = [v, valArray[0] ?? values[0]?.name, ...valArray.slice(2)];
      handleOnChange(listsAsArrays ? val : joinWith(val, ','));
    };

    return (
      <span data-testid={testID} className={className} title={title}>
        <MantineValueSelector
          {...props}
          className={standardClassnames.valueListItem}
          handleOnChange={selector1Handler}
          disabled={disabled}
          value={valArray[0]}
          options={values}
          listsAsArrays={listsAsArrays}
        />
        <MantineValueSelector
          {...props}
          className={standardClassnames.valueListItem}
          handleOnChange={selector2Handler}
          disabled={disabled}
          value={valArray[1]}
          options={values}
          listsAsArrays={listsAsArrays}
        />
      </span>
    );
  }

  switch (type) {
    case 'select':
      return (
        <MantineValueSelector
          {...props}
          className={className}
          title={title}
          value={value}
          disabled={disabled}
          handleOnChange={handleOnChange}
          options={values}
        />
      );
    case 'multiselect':
      return (
        <MantineValueSelector
          {...props}
          className={className}
          title={title}
          value={value}
          disabled={disabled}
          handleOnChange={handleOnChange}
          options={values}
          multiple
          listsAsArrays={listsAsArrays}
        />
      );
    case 'textarea':
      return (
        <Textarea
          {...props}
          value={value}
          title={title}
          size="xs"
          variant="filled"
          disabled={disabled}
          className={className}
          placeholder={placeholder}
          onChange={(e) => handleOnChange(e.currentTarget.value)}
        />
      );
    case 'switch':
      return (
        <Switch
          className={className}
          checked={!!value}
          title={title}
          size="sm"
          disabled={disabled}
          onChange={(e) => handleOnChange(e.currentTarget.value === 'on')}
        />
      );
    case 'checkbox':
      return (
        <Checkbox
          className={className}
          title={title}
          size="sm"
          disabled={disabled}
          onChange={(e) => handleOnChange(e.currentTarget.value === 'on')}
          checked={!!value}
        />
      );
    case 'radio':
      return (
        <Radio.Group
          className={className}
          title={title}
          value={value}
          onChange={handleOnChange}
        >
          {values.map((v: RadioItemValue) => (
            <Radio
              key={`radio-item-value-${v.value || Math.random()}`}
              disabled={disabled}
              value={v.value}
              label={v.label}
            />
          ))}
        </Radio.Group>
      );
    default:
      if (inputType === 'date') {
        return (
          <DatePicker
            value={dayjs(value, 'YYYY-MM-DD').toDate()}
            title={title}
            size="xs"
            variant="filled"
            disabled={disabled}
            className={className}
            placeholder={placeholder}
            onChange={(e) => handleOnChange(dayjs(e).format('YYYY-MM-DD'))}
          />
        );
      }

      if (inputType === 'number') {
        return (
          <NumberInput
            value={parseFloat(value || '0')}
            title={title}
            size="xs"
            variant="filled"
            disabled={disabled}
            className={className}
            placeholder={placeholder}
            onChange={(e) => handleOnChange(e?.toString())}
          />
        );
      }

      return (
        <Input
          type={inputTypeCoerced}
          value={value}
          title={title}
          size="xs"
          variant="filled"
          disabled={disabled}
          className={className}
          placeholder={placeholder}
          onChange={(e: React.FormEvent<HTMLInputElement>) =>
            handleOnChange(e.currentTarget.value)
          }
        />
      );
  }
}

MantineValueEditor.displayName = 'MantineValueEditor';
