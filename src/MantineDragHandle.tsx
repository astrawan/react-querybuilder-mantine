import React from 'react';

import { ActionIcon, type ActionIconProps } from '@mantine/core';

import { IconGripVertical } from '@tabler/icons';

import type { DragHandleProps } from '@react-querybuilder/ts';

export type MantineDragHandleProps = DragHandleProps & ActionIconProps;

export const MantineDragHandle = React.forwardRef<
  HTMLSpanElement,
  MantineDragHandleProps
>(
  (
    {
      className,
      title,
      disabled,
      testID: _testID,
      level: _level,
      path: _path,
      label: _label,
      context: _context,
      validation: _validation,
      ...props
    }: MantineDragHandleProps,
    ref: any
  ) => (
    <span ref={ref} className={className} title={title}>
      <ActionIcon
        disabled={disabled}
        size="xs"
        aria-label={title ?? ''}
        {...props}
        sx={{
          cursor: 'move',
        }}
        variant="transparent"
      >
        <IconGripVertical size={16} />
      </ActionIcon>
    </span>
  )
);

MantineDragHandle.displayName = 'MantineDragHandle';
