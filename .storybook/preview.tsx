import React from 'react';

import type { Parameters } from '@storybook/api';

import { useDarkMode } from 'storybook-dark-mode';

import { type ColorScheme, MantineProvider } from '@mantine/core';

function Wrapper({ children }: { children: React.ReactNode }) {
  const colorScheme: ColorScheme = useDarkMode() ? 'dark' : 'light';

  return (
    <MantineProvider theme={{ colorScheme }} withGlobalStyles withNormalizeCSS>
      {children}
    </MantineProvider>
  );
}

export const parameters: Parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
}

export const decorators = [
  (renderStore: () => React.ReactNode) => (
    <Wrapper>{renderStore()}</Wrapper>
  ),
];
