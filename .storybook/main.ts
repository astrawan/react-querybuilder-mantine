import path from 'path';

import type { Options, StorybookConfig } from '@storybook/core-common';

const config: StorybookConfig = {
  addons: [
    '@storybook/addon-links',
    '@storybook/addon-essentials',
    '@storybook/addon-interactions',
    'storybook-dark-mode',
  ],
  core: {
    builder: "@storybook/builder-webpack5",
  },
  framework: '@storybook/react',
  stories: [
    '../src/**/*.stories.mdx',
    '../src/**/*.stories.@(js|jsx|ts|tsx)',
  ],
  webpackFinal: async (cfg, { configType }: Options) => {
    cfg.module?.rules?.push({
      test: /\.scss$/,
      use: ['style-loader', 'css-loader', 'sass-loader'],
      include: path.resolve(__dirname, '../'),
    })
    return cfg;
  },
}
module.exports = config
