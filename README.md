## react-querybuilder-mantine

Unofficial [react-querybuilder](https://npmjs.com/package/react-querybuilder) components for [Mantine UI](https://mantine.dev/).

## Installation

```bash
npm i --save react-querybuilder dayjs @tabler/icons @mantine/core @mantine/dates react-querybuilder-mantine
# OR
yarn add react-querybuilder dayjs @tabler/icons @mantine/core @mantine/dates react-querybuilder-mantine
```

## Usage

```tsx
import React from "react";

import {
  type ColorScheme,
  MantineProvider,
  Stack,
  Textarea,
} from "@mantine/core";
import { mantineControlElements } from "react-querybuilder-mantine";
import {
  type Field,
  QueryBuilder,
  type RuleGroupType,
} from "react-querybuilder";
import "react-querybuilder/dist/query-builder.scss";

const fields: Field[] = [
  { name: "firstName", label: "First Name" },
  { name: "lastName", label: "Last Name" },
];

function App() {
  const colorScheme: ColorScheme = "dark";
  const [query, setQuery] = React.useState<RuleGroupType>({
    combinator: "and",
    rules: [],
  });

  return (
    <MantineProvider theme={{ colorScheme }} withGlobalStyles withNormalizeCSS>
      <Stack p="xs">
        <QueryBuilder
          fields={fields}
          query={query}
          onQueryChange={(q) => setQuery(q)}
          controlElements={mantineControlElements}
        />
        <Textarea autosize readOnly value={JSON.stringify(query, null, "  ")} />
      </Stack>
    </MantineProvider>
  );
}

export default App;
```

## Demo

```bash
npm run storybook
```

