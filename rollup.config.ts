import resolve from '@rollup/plugin-node-resolve';
import commonjs from '@rollup/plugin-commonjs';
import typescript from '@rollup/plugin-typescript';

import type { RollupOptions } from 'rollup';

// Remember enable compilerOptions > resolveJsonModule = true option
// in tsconfig.json
import * as pkg from './package.json';

const configs: RollupOptions[] = [
  {
    input: 'src/index.ts',
    output: [
      {
        file: pkg.main,
        format: 'cjs',
        exports: 'named',
        sourcemap: true,
      },
      {
        file: pkg.module,
        format: 'esm',
        exports: 'named',
        sourcemap: true,
      },
    ],
    plugins: [
      resolve(),
      commonjs(),
      typescript({ tsconfig: './tsconfig.lib.json' }),
    ],
    external: [...Object.keys(pkg.peerDependencies || {})],
  },
];

export default configs;
